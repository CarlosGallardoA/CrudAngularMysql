import express, { Application } from "express";
import indexRoutes from "./routes/indexRoutes";
import gamesRoutes from "./routes/gamesRoutes";
import morgan from "morgan";
import cors from "cors";

class Server {
  public app: Application;
  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }
  config(): void {
    //configura el app
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(morgan("dev"));
    this.app.use(cors());
    this.app.use(express.json()); //express pueda entender los formatos json ya no se necesita body-parser
    this.app.use(express.urlencoded({ extended: false }));
  }
  routes(): void {
    //definir las rutas de app
    this.app.use("/", indexRoutes);
    this.app.use("/api/games", gamesRoutes);
  }
  start(): void {
    //Inicializar el servidor
    this.app.listen(this.app.get("port"), () => {
      console.log(`Server on port ${this.app.get("port")}`);
    });
  }
}

const server = new Server();
server.start();
